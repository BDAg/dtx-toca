﻿const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
 
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*'); // allow access from everywhere
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET'); // Quais os métodos possíveis
        return res.status(200).json({});
    }

    next();
});
 
// Conexão com mongo
mongoose.connect('mongodb://10.64.191.192/toca');
const casasRota = require('./rotas/casasRotas');
app.use("/casas", casasRota);
 
//const houseRoute = require('./routes/houseRoute');
 
// app.use("/houses", houseRoute);
 
app.use("/",(req, res, next) => {
    res.status(404).send({"msg":"not found"})
});
 
module.exports = app;