const casa = require('../modelos/casas');

exports.listaCasas = (req, res, next) => {
    casa.find({})
    .exec()
    .then(casas => {
        res.status(200).send({
            response: casas
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).send({
            error: err
        });
    });

};