const mongoose = require('mongoose');

const casaSchema = mongoose.Schema({
    _id: mongoose.SchemaTypes.ObjectId,
    nome: String,

})

module.exports = mongoose.model('Casa', casaSchema, 'casas');