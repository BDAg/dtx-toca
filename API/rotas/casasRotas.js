const express = require('express');
const router = express.Router();

const casaControle = require('../controles/casasControle');

router.get('/', casaControle.listaCasas);

module.exports = router;