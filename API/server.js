//
//Busca referencia do http
const http = require("http");
const app = require('./app');

//Cria o server
const server = http.createServer(app);

//"Escuta" o server na porta definida anteriormente
server.listen(3000);