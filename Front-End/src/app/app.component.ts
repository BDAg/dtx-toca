import { Component } from '@angular/core';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root', 
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
  })

export class AppComponent {

  array = [];

  title = 'toca';
  faCoffe = faCoffee
  constructor() {}

  enviarPrint(form: any | FormGroup) {
    this.array = [];
    for (const field in form.controls) {
        const control = form.controls[field];
        if(control.value === true) {
          this.array.push(field);
        }
    }
    console.log(this.array);
  }
}
