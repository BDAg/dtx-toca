import { Component, OnInit } from '@angular/core';
import {ApiService} from '../api.service'

@Component({
  selector: 'app-casas',
  templateUrl: './casas.component.html',
  styleUrls: ['./casas.component.css']
})
export class CasasComponent implements OnInit {

  casas: any = [];


  constructor(private api: ApiService) { }

  ngOnInit() {
  }

  click(){
    this.api.listasCasas().subscribe(dados => {
      console.log(dados)
      this.casas = dados["response"];
      console.log(this.casas)
    });
  }
}
