import { Component, OnInit } from '@angular/core';

import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-filtro',
  templateUrl: './filtro.component.html',
  styleUrls: ['./filtro.component.css']
})
export class FiltroComponent implements OnInit {

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  constructor() { }

  ngOnInit() {

    this.dropdownList = [
      { item_id: 1, item_text: 'Geladeira' },
      { item_id: 2, item_text: 'Fogão' },
      { item_id: 3, item_text: 'Armários' },
      { item_id: 4, item_text: 'Cama' },
      { item_id: 5, item_text: 'Ar condicionado' }
    ];

    // this.selectedItems = [
    //   { item_id: 3, item_text: 'Pune' },
    //   { item_id: 4, item_text: 'Navsari' }
    // ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Selecione tudo',
      unSelectAllText: 'Cancelar',
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

}

