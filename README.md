# Toca Imóveis

Esse projeto tem como o objetivo agrupar serviços e melhorar a experiência do usuário na hora que alugar um imóvel.


## Documentação
- [Cronograma](https://gitlab.com/BDAg/dtx-toca/wikis/Cronograma)
- [Mapa de Conhecimento](https://gitlab.com/BDAg/dtx-toca/wikis/Mapa-de-Conhecimento)
- [Matriz de Habilidades](https://gitlab.com/BDAg/dtx-toca/wikis/Matriz-de-Habilidades)
- [Team](https://gitlab.com/BDAg/dtx-toca/wikis/Team)
- [MVP - Minimal Viable Product](https://gitlab.com/BDAg/dtx-toca/wikis/MVP)